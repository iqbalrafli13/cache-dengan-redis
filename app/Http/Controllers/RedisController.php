<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Belajarredis;
use Illuminate\Support\Facades\Cache;


class RedisController extends Controller
{
    public function tanparedis($value='')
    {
        $blogs = Belajarredis::all();
        $title = 'tanpa redis';

        return view('blog', compact('blogs','title'));
    }

    public function denganredis($value='')
    {
        $title = 'dengan redis';
        $blogs = Cache::remember('blog',10,function () {
            return Belajarredis::all();
        });
        return view('blog', compact('blogs','title'));
    }
}
